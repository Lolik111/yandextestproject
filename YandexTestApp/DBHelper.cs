﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Policy;
using System.Text;

namespace YandexTestApp
{
    public class DBHelper
    {
        public SQLiteConnection Connection { get; private set; }

        public const string DatabaseName = "default.db";

        private string preparedString;

        private string[] _columns;

        private DTypes[] types = new DTypes[4];

        private SQLiteTransaction transaction;

        public string[] Columns
        {
            get
            {
                return _columns;
            }
            set
            {
                _columns = value;
                for (int i = 0; i < 4; ++i)
                {
                    switch (_columns[i])
                    {
                        case "id":
                        case "product_id":
                            types[i] = DTypes.IntNum;
                            break;
                        case "amount":
                            types[i] = DTypes.DoubleNum;
                            break;
                        case "dt":
                            types[i] = DTypes.DateTime;
                            break;
                    }
                }
            }
        }
        
        public void Open()
        {
            string folder = Environment.CurrentDirectory;
            if (!File.Exists(Path.Combine(folder, DatabaseName)))
            {
                SQLiteConnection.CreateFile(DatabaseName);
            }

            Connection = new SQLiteConnection(string.Format("Data Source = {0}", Path.Combine(folder, DatabaseName))).OpenAndReturn();   
            CreateTables();
        }

        public void Commit()
        {
            transaction.Commit();
            transaction.Dispose();
        }

        public void Close()
        {
            Connection.Close();
            Connection.Dispose();
        }

        public string GetThirdQueryResult()
        {
            using (SQLiteCommand command = new SQLiteCommand(SqlThirdTaskQuery, Connection))
            {
                var reader = command.ExecuteReader();
                var sb = new StringBuilder();
                var info = new DateTimeFormatInfo();
                sb.AppendLine("Период   Продукт    Сумма   Доля");
                while (reader.Read())
                {
                    sb.AppendLine(
                        string.Format(
                            "{0} {1} {2} {3}%", info.GetMonthName(reader.GetInt32(1)) + " " + reader.GetString(0), reader.GetString(2), reader.GetDouble(3), reader.GetDouble(4) * 100));
                }
                return sb.ToString();
            }
        }

        public string GetSecondQueryResult()
        {
            
            StringBuilder sb = new StringBuilder();
            using (SQLiteCommand command = new SQLiteCommand(SqlSecondTaskAQuery, Connection))
            {
                var reader = command.ExecuteReader();
                sb.Append("Продукты, заказанные в текущем, но не в прошлом месяце: ");
                if (reader.Read())
                {
                    sb.AppendFormat("{0}", reader.GetString(0));
                }
                while (reader.Read())
                {
                    sb.AppendFormat(", {0}", reader.GetString(0));
                }
                sb.Append(".");
            }
            sb.AppendLine();
            using (SQLiteCommand command = new SQLiteCommand(SqlSecondTaskBQuery, Connection))
            {
                var reader = command.ExecuteReader();
                sb.Append("Продукты, заказанные в прошлом, но не в текущем месяце, и наоборот: ");
                if (reader.Read())
                {
                    sb.AppendFormat("{0}", reader.GetString(0));
                }
                while (reader.Read())
                {
                    sb.AppendFormat(", {0}", reader.GetString(0));
                }
                sb.Append(".");
                sb.AppendLine();
            }
            return sb.ToString();
        }

        public string GetFirstQueryResult()
        {
            using (SQLiteCommand command = new SQLiteCommand(SqlFirstTaskQuery, Connection))
            {
                var reader = command.ExecuteReader();
                StringBuilder sb = new StringBuilder();
                while (reader.Read())
                {
                    sb.AppendLine(
                        string.Format(
                            "Продукт: {0}, количество заказов: {1}, сумма заказов: {2}",
                            reader.GetString(0), reader.GetInt32(1),
                            reader.GetDouble(2)));
                }
                return sb.ToString();
            }
        }

        public bool TryInsert(string[] data, out string e)
        {
            if (preparedString == null)
            {
                preparedString =
                    string.Format(
                        "INSERT INTO [order] ([{0}], [{1}], [{2}], [{3}]) VALUES (@param1, @param2, @param3, @param4)", Columns[0], Columns[1], Columns[2], Columns[3]);
            }
            if (transaction == null)
            {
                transaction = Connection.BeginTransaction();
            }
            using (SQLiteCommand command =
                    new SQLiteCommand(preparedString, Connection, transaction))
            {
                command.Parameters.Add(new SQLiteParameter("@param1", data[0]));
                command.Parameters.Add(new SQLiteParameter("@param2", data[1]));
                command.Parameters.Add(new SQLiteParameter("@param3", data[2]));
                command.Parameters.Add(new SQLiteParameter("@param4", data[3]));
                try
                {
                    if (command.ExecuteNonQuery() == 0)
                    {
                        e = "Unknown error";
                        return false;
                    }

                }
                catch (Exception exception)
                {
                    e = exception.Message;
                    transaction.Commit();
                    transaction.Dispose();
                    transaction = null; 
                    return false;
                }
            }
            e = null;
            return true;
        }

        public bool Check(string[] data, out string exc)
        {
            if (data.Length < 4)
            {
                exc = "Invalid columns number";
                return false;
            }
            for (int i = 0; i < 4; ++i)
            {
                if (this.types[i].Equals(DTypes.IntNum))
                {
                    int t;
                    if (!int.TryParse(data[i], out t))
                    {
                        exc = string.Format("Invalid {0} format: int required", Columns[i]);
                        return false;
                    }
                }
                else if (this.types[i].Equals(DTypes.DateTime))
                {
                    DateTime dt;
                    if (!DateTime.TryParse(data[i], out dt))
                    {
                        exc = string.Format("Invalid {0} format: datetime required", Columns[i]);
                        return false;
                    }
                }
                else if (this.types[i].Equals(DTypes.DoubleNum))
                {
                    double d;
                    if(!double.TryParse(data[i].Replace('.', ','), out d))
                    {
                        exc = string.Format("Invalid {0} format: real required", Columns[i]);
                        return false;
                    }
                }
            }
            exc = null;
            return true;
        }

        private void CreateTables()
        {  using (SQLiteCommand command = new SQLiteCommand(SqlCheck, Connection))
            {
                var reader = command.ExecuteReader();
                reader.Read();
                if (reader.GetInt32(0) == 0)
                {
                    using (SQLiteCommand createTableCommand = new SQLiteCommand(SqlCreateProduct, Connection))
                    {
                        createTableCommand.ExecuteScalar();
                    }
                    using (SQLiteCommand insertCommand = new SQLiteCommand(SqlInsertProduct, Connection))
                    {
                        insertCommand.ExecuteNonQuery();
                    }

                }
            }

            using (SQLiteCommand createOrderCommand = new SQLiteCommand(SqlCreateOrder, Connection))
            {
                createOrderCommand.ExecuteScalar();
            }
        }

        enum DTypes
        {
            IntNum,
            DoubleNum,
            DateTime
        }

        #region queries

        private const string SqlCheck = @"SELECT count(*) as count FROM sqlite_master WHERE type='table' AND name='product'";
        private const string SqlCreateProduct = @"CREATE TABLE IF NOT EXISTS [product] (id integer, name text)";
        private const string SqlInsertProduct =
            @"INSERT INTO [product] VALUES (1, 'A'); 
                INSERT INTO [product] VALUES (2, 'B');
                INSERT INTO [product] VALUES (3, 'C'); 
                INSERT INTO [product] VALUES (4, 'D'); 
                INSERT INTO [product] VALUES (5, 'E'); 
                INSERT INTO [product] VALUES (6, 'F'); 
                INSERT INTO [product] VALUES (7, 'G');";
        private const string SqlCreateOrder = @"CREATE TABLE IF NOT EXISTS [order] (
                                           id integer,
                                           dt datetime, 
                                           product_id integer,
                                           amount real)";

        private const string SqlFirstTaskQuery = @"SELECT t1.name, COUNT(t2.id), SUM(t2.amount)
                            FROM [product] t1 JOIN [order] t2 ON t1.id = t2.product_id
                            WHERE strftime('%m', t2.dt) = strftime('%m', 'now') AND strftime('%Y', t2.dt) = strftime('%Y', 'now') 
                            GROUP BY t1.id";
        private const string SqlSecondTaskAQuery = @"SELECT t1.name
                            FROM [product] t1
                            WHERE EXISTS
                            	(SELECT * 
                            	FROM [order] t3
                            	WHERE t1.id = t3.product_id AND strftime('%Y', t3.dt) = strftime('%Y', 'now') AND strftime('%m', 'now') = strftime('%m', t3.dt) 
                            	)
                            	AND NOT EXISTS 
                            	(SELECT * 
                            	FROM [order] t2
                            	WHERE t1.id = t2.product_id AND ((strftime('%Y', t2.dt) = strftime('%Y', 'now') AND strftime('%m', 'now') - strftime('%m', t2.dt) = 1) OR
                            	(strftime('%Y', 'now') - strftime('%Y', t2.dt) = 1 AND strftime('%m', 'now') - strftime('%m', t2.dt) = -11))
                            	)";
        private const string SqlSecondTaskBQuery = @"SELECT t1.name
                                    FROM [product] t1
                                    WHERE EXISTS
                                    	(SELECT * 
                                    	FROM [order] t3
                                    	WHERE t1.id = t3.product_id AND strftime('%Y', t3.dt) = strftime('%Y', 'now') AND strftime('%m', 'now') = strftime('%m', t3.dt) 
                                    	)
                                    	AND NOT EXISTS 
                                    	(SELECT * 
                                    	FROM [order] t2
                                    	WHERE t1.id = t2.product_id AND ((strftime('%Y', t2.dt) = strftime('%Y', 'now') AND strftime('%m', 'now') - strftime('%m', t2.dt) = 1) OR
                                    	(strftime('%Y', 'now') - strftime('%Y', t2.dt) = 1 AND strftime('%m', 'now') - strftime('%m', t2.dt) = -11))
                                    	)
                                    	OR NOT EXISTS
                                    	(SELECT * 
                                    	FROM [order] t3
                                    	WHERE t1.id = t3.product_id AND strftime('%Y', t3.dt) = strftime('%Y', 'now') AND strftime('%m', 'now') = strftime('%m', t3.dt) 
                                    	)
                                    	AND EXISTS 
                                    	(SELECT * 
                                    	FROM [order] t2
                                    	WHERE t1.id = t2.product_id AND ((strftime('%Y', t2.dt) = strftime('%Y', 'now') AND strftime('%m', 'now') - strftime('%m', t2.dt) = 1) OR
                                    	(strftime('%Y', 'now') - strftime('%Y', t2.dt) = 1 AND strftime('%m', 'now') - strftime('%m', t2.dt) = -11))
                                    	)";
        private const string SqlThirdTaskQuery = @"SELECT t3.year, cast(t3.month as integer), t3.name, MAX(t3.ssum), t3.ssum/t4.periodSum as part 
                                                FROM 
                                                (SELECT strftime('%m', t1.dt) as month, strftime('%Y', t1.dt) as year, t2.name, t2.id as id, SUM(amount) as ssum
                                                FROM [order] t1 JOIN [product] t2 ON t1.product_id = t2.id
                                                GROUP BY strftime('%m', t1.dt), strftime('%Y', t1.dt), t2.id
                                                ) t3 JOIN 
                                                (SELECT strftime('%m', t1.dt) as month, strftime('%Y', t1.dt) as year,  SUM(amount) as periodSum
                                                FROM [order] t1 JOIN [product] t2 ON t1.product_id = t2.id
                                                GROUP BY strftime('%m', t1.dt), strftime('%Y', t1.dt)
                                                ) t4 ON t3.month = t4.month AND t3.year = t4.year
                                                GROUP BY t3.year, t3.month
                                                order by t3.year, t3.year";
    }

        #endregion
}