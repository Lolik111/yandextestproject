﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;


namespace YandexTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName;
            if (args.Length == 0)
            {
                Console.WriteLine("Enter file name");
                fileName = Console.ReadLine();
            }
            else
            {
                fileName = args[0];
            }
            if (!File.Exists(fileName))
            {
                Console.WriteLine("Please provide required file");
                return;
            }
            
            DBHelper helper = new DBHelper();
            helper.Open();
            
            using (FileStream fs = File.Open(fileName, FileMode.Open))
            using (BufferedStream bs = new BufferedStream(fs))
            using (StreamReader sr = new StreamReader(bs))
            {
                string input;
                string[] columns;
                try
                {
                    columns = sr.ReadLine().Split();
                    if (columns.Length < 4)
                    {
                        Console.WriteLine("Something went wrong");
                        Console.Read();
                        return;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong");
                    Console.Write(e.Message);
                    Console.Read();
                    return;
                }
                helper.Columns = columns;
                int line = 1;
                while ((input = sr.ReadLine()) != null)
                {
                    var data = input.Split(new[] {'\t'});
                    string exc;
                    if (!helper.Check(data, out exc))
                    {
                        Console.WriteLine("Line {0}, {1}", line, exc);
                    }
                    else if (!helper.TryInsert(data, out exc))
                    {
                        Console.WriteLine("Line {0}, {1}", line, exc);
                    }
                    line++;
                }
            }
            helper.Commit();
            Console.WriteLine("First task");
            Console.Write(helper.GetFirstQueryResult());
            Console.WriteLine("Second task");
            Console.Write(helper.GetSecondQueryResult());
            Console.WriteLine("Third task");
            Console.Write(helper.GetThirdQueryResult());
            helper.Close();
            Console.Read();
        }

       
    }
}
